import httpV1 from "src/services/httpV1";

export function getTickets() {
  return httpV1({
    url: 'tickets',
    method: 'GET',
  });
}
export function getTicketById(id: string) {
  return httpV1({
    url: `tickets/${id}`,
    method: 'GET'
  });
}
export function filterTicketsByStatus(status: string) {
  return httpV1({
    url: `tickets?status=${status}`,
    method: 'GET'
  });
}