import { Container, Typography } from "@mui/material";
import { Outlet } from "react-router-dom";

export default function Layout() {
  return (
    <Container>
      <Typography m="50px auto" align="center" component="h1" variant="h4" >This is Header</Typography>
      <Outlet />
    </Container>
  );
}
