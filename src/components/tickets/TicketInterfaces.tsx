import { SelectChangeEvent } from "@mui/material";

export interface TicketDataProps {
    number: string;
    status: string;
    id: number;
}
export interface TicketsFilterProps {
    handleFilter: (event: SelectChangeEvent) => Promise<void>;
    selected: string,
    statusList: string[]
}
