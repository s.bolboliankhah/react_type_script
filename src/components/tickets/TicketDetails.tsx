import { useEffect, useState } from 'react';
import { Box, Card, CardContent, Typography } from '@mui/material';
import { useParams } from 'react-router-dom';
import { getTicketById } from 'src/apis/tickets';
import { TicketDataProps } from "./TicketInterfaces"
import GoBack from '../utiles/GoBack';
import LoadingScreen from '../utiles/LoadingScreen';

export default function TicketDetails() {
    const { id } = useParams()
    const [loading, setLoading] = useState(false);
    const [data, setDate] = useState<TicketDataProps>();

    const FetchData = async () => {
        if (!id) return
        setLoading(true)
        const res = await getTicketById(id)
        if (res.success) {
            setDate(res.dataBody)
        }
        setLoading(false)
    }

    useEffect(() => {
        async function fetch() {
            await FetchData();
        }
        fetch();
    }, [])

    return (
        <Box m="10px auto" width="100%" bgcolor="center" display="grid">
            <GoBack />
            {
                loading
                    ? <LoadingScreen />
                    : <Box mx="auto" maxWidth="400px" minWidth="200px">
                        {data
                            ? <Card sx={{ m: "auto" }}>
                                <CardContent sx={{ height: "170px", p: "30px" }}>
                                    <Typography gutterBottom align='center' >
                                        {data.number}
                                    </Typography>
                                    <Typography align='center' >
                                        {data.status}
                                    </Typography>
                                </CardContent>
                            </Card>
                            : "Not Found"}
                    </Box>
            }
        </Box>
    );
}