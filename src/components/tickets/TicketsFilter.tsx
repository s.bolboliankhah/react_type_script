import { FormControl, MenuItem, Select, SelectChangeEvent } from '@mui/material';
import { TicketsFilterProps } from "./TicketInterfaces"

export default function TicketsFilter(props: TicketsFilterProps) {

    return (
        <FormControl sx={{ m: 1, minWidth: 150 }}>
            <Select
                value={props.selected}
                onChange={props.handleFilter}
            >
                {
                    props.statusList.map((el, index) => <MenuItem key={index} value={el}>{el}</MenuItem>)
                }
            </Select>
        </FormControl>
    );
}