import { Button, Card, CardActions, CardContent, Grid, Typography } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { TicketDataProps } from "./TicketInterfaces"

interface TicketCardProps {
    data: TicketDataProps;
}

export default function TicketCard(props: TicketCardProps) {
    const { number, id, status } = props.data
    const navigate = useNavigate()
    return (
        <Grid item xs={12} sm={6} md={4} lg={3}>
            <Card
                sx={{
                    display: "grid",
                    justifyContent: "flex",
                    alignItems: "center",
                    bgcolor: "secondary.light",
                    minWidth:"200px"
                }}
            >
                <CardContent sx={{ height: "70px" }}>
                    <Typography gutterBottom variant="h4">
                        {number}
                    </Typography>
                    <Typography gutterBottom variant='body1' color="primary.light">
                        {status}
                    </Typography>
                </CardContent>
                <CardActions
                    sx={{
                        display: "flex",
                        justifyContent: "end",
                        alignItems: "end",
                    }}
                >
                    <Button size="small" onClick={() => navigate(`/app/tickets/${id}`)}>Details</Button>
                </CardActions>
            </Card>
        </Grid>
    );
}