import { useEffect, useState } from 'react';
import { filterTicketsByStatus, getTickets } from 'src/apis/tickets';
import { Box, Grid, Pagination, SelectChangeEvent, Stack } from '@mui/material';
import SkeletonThreeLine from 'src/components/utiles/SkeletonThreeLine';
import TicketCard from './TicketCard';
import { TicketDataProps } from "./TicketInterfaces"
import TicketsFilter from './TicketsFilter';

export default function TicketsList() {

    const statusList = ["All", "Assigned", "Completed", "UnAssigned"]
    const [loading, setLoading] = useState(false);
    const [totalPages, setTotalPages] = useState(10);
    const [page, setPage] = useState(1);
    const [totallist, setTotalList] = useState([]);
    const [list, setList] = useState([]);
    const [selected, setSelected] = useState(statusList[0]);

    const countItemsPerPage = 12

    const FetchData = async () => {
        setLoading(true)
        const data = await getTickets()
        if (data.success) {
            setTotalList(data.dataBody)
            setList(data.dataBody.slice(0, countItemsPerPage))
            setTotalPages(Math.ceil(data.dataBody.length / countItemsPerPage))
        }
        setLoading(false)
    }
    useEffect(() => {
        async function fetch() {
            await FetchData();
        }
        fetch();
    }, [])

    const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
        setPage(value);
        setList(totallist.slice((value - 1) * countItemsPerPage, value * countItemsPerPage))
    };

    const handleFilter = async (event: SelectChangeEvent) => {
        setLoading(true)
        const val = event.target.value
        setSelected(val)
        if (val == "All") {
            await FetchData()
        } else {
            const data = await filterTicketsByStatus(val.toLocaleLowerCase())
            if (data.success) {
                setTotalList(data.dataBody)
                setList(data.dataBody.slice(0, countItemsPerPage))
                setTotalPages(Math.ceil(data.dataBody.length / countItemsPerPage))
            }
        }
        setLoading(false)
    };

    return (
        <Box m={{ xs: 2, sm: 6, md: 10 }}>
            {loading ? <SkeletonThreeLine countItemsPerPage={countItemsPerPage} />
                :
                <>
                    <TicketsFilter handleFilter={handleFilter} selected={selected} statusList={statusList} />
                    <Grid container spacing={2}> {
                        list.map((el: TicketDataProps, index) => (
                            <TicketCard key={index} data={el} />
                        ))}
                    </Grid>
                    <Stack spacing={2} pt={5} >
                        <Pagination sx={{ display: "flex", justifyContent: "center", alignContent: "center" }} count={totalPages} page={page} onChange={handleChange} shape="rounded" />
                    </Stack>
                </>
            }
        </Box>
    );
}