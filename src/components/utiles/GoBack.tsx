import { Box, Button } from "@mui/material";
import { useNavigate } from "react-router-dom";

export default function GoBack() {
    const navigate = useNavigate()
    return (
        <Box width="100%" justifyContent="flex-end">
            <Button onClick={() => navigate(-1)}>Go Back</Button>
        </Box>
    );
}
