import { Box, CircularProgress } from "@mui/material";

export default function LoadingScreen() {
    return (
        <Box width="100%" display="flex" justifyContent="center"> 
            <CircularProgress />
        </Box>
    );
}
