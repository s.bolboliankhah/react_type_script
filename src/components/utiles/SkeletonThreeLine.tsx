import { Box, Grid, Skeleton } from "@mui/material";

interface SkeletonProps {
    countItemsPerPage: number;
}
export default function SkeletonThreeLine(props: SkeletonProps) {
    return (
        <Grid container spacing={2}>
            {
                Array.from(new Array(props.countItemsPerPage)).map((el, index) => (
                    <Grid key={index} item xs={6} md={4} lg={3}>
                        <Box p={3} >
                            <Skeleton />
                            <Skeleton animation="wave" />
                            <Skeleton animation={false} />
                        </Box>
                    </Grid>
                ))
            }
        </Grid>
    );
}
