import axios, { AxiosInstance, AxiosRequestConfig, AxiosError } from 'axios';

export const BaseUrlV1 = "http://localhost:3004/";

const Axios: AxiosInstance = axios.create({
    baseURL: BaseUrlV1,
});

export default async function http({ method, url, data }: AxiosRequestConfig): Promise<{
  //  message: string,
    status: number,
    dataBody: any,
    success: boolean
}> {
    try {
        const response = await Axios.request({
            method,
            url,
            data
        });
        if (response.status !== 200) {
            return {
                success: false,
               // message: response.data?.error?.message,
                status: response.status,
                dataBody: {},
            };
        }
        return {
            success: true,
         //   message: response.data.message,
            status: response.status,
            dataBody: response.data,
        };
    } catch (error: AxiosError | any) {
        console.log("==========",error)
        const status = error.response?.status;
        if (status === 401) {
            // to do logout
        }
        return {
            success: false,
         //   message: error.response?.data?.message || "something wrong",
            status: status || 500,
            dataBody: {},
        };
    }
}
