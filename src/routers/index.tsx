import { Navigate, Outlet, RouterProvider, createBrowserRouter } from 'react-router-dom';
import Layout from 'src/components/layout';
import TicketDetails from 'src/components/tickets/TicketDetails';
import TicketsList from 'src/components/tickets/TicketsList';

export default function Router() {

    const routes = createBrowserRouter([
        { path: '/', element: <Navigate to="/app/tickets" /> },
        {
            path: 'app',
            element: <Layout />,
            children: [
                {
                    path: 'tickets',
                    element: <Outlet />,
                    children: [
                        { path: '', index: true, element: <TicketsList /> },
                        { path: ':id', element: <TicketDetails /> },
                    ],
                }
            ],
        },
        //{ path: '*', element: <NotFound /> },
    ]);

    return <RouterProvider router={routes} />;
}
