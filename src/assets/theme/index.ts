import { createTheme } from "@mui/material/styles";
import { ThemeOptions } from "@mui/material/styles";

const lightMode: ThemeOptions = {
  palette: {
    mode: "light",
    common: {
      white: "#ffffff",
      black: "#000000",
    },
    primary: {
      main: "#36ACE2",
      light: "#0F3661",
      dark: "#02254D",
    },
    secondary: {
      main: "#4F6087",
      light: "#F1F1F1",
      dark :"#E7E5E5"
    },
    info: {
      main: "#F7F7F7",
      light: "#F3F3F3",
      dark :"#ffffff"
    },
    text: {
      primary: "#0F3661",
      secondary: "#004D88",
      disabled: "#aeaeae"
    },
    action: {
      active: "#ffffff",
    },
    background: {
      paper: "#F7F7F7",
      default: "#F5F5F5",
    },
  },
};

const overrides = {
  typography: {
    h1: {
      fontWeight: 700,
      fontSize: "3.5rem",
      lineHeight: 1.167,
    },
    h2: {
      fontWeight: 700,
      fontSize: "2.6rem",
      lineHeight: 1.167,
    },
    h3: {
      fontWeight: 700,
      fontSize: "2.3rem",
      lineHeight: 1.167,
    },
    h4: {
      fontWeight: 700,
      fontSize: "1.6rem",
      lineHeight: 1.235,
    },
    body1: {
      fontWeight: 400,
      fontSize: "1rem",
      lineHeight: 1.5,
    },
    fontFamily: `"sans-serif", "serif" `,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    fontWeightBold: 700,
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
  },
  customShadows: {
  },
};

const lightTheme = createTheme({ ...lightMode, ...overrides });

export { lightTheme };
