import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './app/app';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { lightTheme } from './assets/theme';

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={lightTheme}>
      <CssBaseline />
      <App />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);

