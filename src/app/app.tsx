import Router from 'src/routers';
import './app.css';

export type AppProps = {};

const App = () => {
  return (
    <Router />
  );
};

export default App;

